package fr.fondespierre.beweb.mobile.apprenants;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import fr.fondespierre.beweb.mobile.apprenants.adapters.ListeApprenantAdapter;
import fr.fondespierre.beweb.mobile.apprenants.dal.Data;

public class ListeApprenantsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste_apprenants);

        final ListView listeApprenant = (ListView) findViewById(R.id.la_listView_apprenants);
        final JSONArray listeData = null;
        final Activity activity = this;

        final Spinner promoSpinner = (Spinner) findViewById(R.id.la_spinner_promo);




        final TextView test = (TextView) findViewById(R.id.testspin);

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://192.168.1.48/beweb_api/index.php/";

        // Request a response from the provided URL.
        JsonArrayRequest apprenantRequest = new JsonArrayRequest(Request.Method.GET, url + "apprenants", listeData,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ListeApprenantAdapter adapter = new ListeApprenantAdapter(activity, R.layout.liste_apprenant_item, response);


                        listeApprenant.setAdapter(adapter);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        // Add the request to the RequestQueue.
        queue.add(apprenantRequest);

        // Request a response from the provided URL.
        JsonArrayRequest villeRequest = new JsonArrayRequest(Request.Method.GET, url + "villes", listeData,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(final JSONArray response) {
                        ArrayList<String> villeslist = new ArrayList<>();
                        villeslist.add("Promotion");
                        for(int i = 0; i< response.length(); i++){
                            try {
                                // get the response in the string format and add it to the arrayList
                                villeslist.add(response.getString(i));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        // populate the spinner with the listArray villeslist
                        promoSpinner.setAdapter(new ArrayAdapter<String>(ListeApprenantsActivity.this,
                                android.R.layout.simple_spinner_dropdown_item,
                                villeslist));

                        // when an item is selected check the text of this item and populate
                        // the session spinner
                        // TODO filter the results according to the promotion
                        promoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                CheckedTextView ville = (CheckedTextView)view ;
                                getSessionsByPromo(ville.getText().toString());


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(villeRequest);

    }

    /**
     *
     *
     * @param ville
     */
    private void getSessionsByPromo(String ville){
        final JSONArray listeData = null;
        final Spinner sessionSpinner = (Spinner) findViewById(R.id.la_spinner_session);
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://192.168.1.48/beweb_api/index.php/";

        // Request a response from the provided URL.
        JsonArrayRequest villeRequest = new JsonArrayRequest(Request.Method.GET, url + "sessions/"+ville, listeData,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ArrayList<String> sessionslist = new ArrayList<>();
                        sessionslist.add("Session");
                        for(int i = 0; i< response.length(); i++){
                            try {
                                // get the response in the string format and add it to the arrayList
                                sessionslist.add(response.getString(i));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        // populate the spinner with the listArray villeslist
                        sessionSpinner.setAdapter(new ArrayAdapter<String>(ListeApprenantsActivity.this,
                                android.R.layout.simple_spinner_dropdown_item,
                                sessionslist));

                        // TODO filter the results according to the session of the previously selected promotion
                        sessionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(villeRequest);

    }

}